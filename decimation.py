import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

time_of_view        = 1.; # s.
dynamic_range       = 1.;

sampling_rate       = 200.; # Hz
sampling_period     = 1. / sampling_rate; # s
sample_number       = time_of_view / sampling_period;
sampling_time       = np.linspace (0, time_of_view, sample_number);

carrier_frequency   = 4.;
amplitude           = 1;
phase               = 0;

decimation_factor   = 10;


sampling_signal     = amplitude * np.cos (2 * np.pi * carrier_frequency * sampling_time + phase);

decimated_time      = sampling_time [0::decimation_factor];
decimated_signal    = sig.decimate (sampling_signal, decimation_factor, 1, ftype="fir", axis=-1);
#                                                                       ^                      ^
#                                                                       |                      |
#                                                    In future versions should be greater.     |
#                                                                  In future versions should be True.

fig = plt.figure ()
plt.plot (sampling_time, sampling_signal);
plt.plot (decimated_time, decimated_signal);
#plt.stem (decimated_time, decimated_signal, linefmt='r-', markerfmt='ro', basefmt='r-');
plt.title("Decimation (downsampling)")
plt.xlabel("Time")
plt.ylabel("Amplitude")

plt.show()