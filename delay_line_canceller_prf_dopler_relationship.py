import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


minDoplerHz = 0;
maxDoplerHz = 400;
doplerHz = np.linspace (minDoplerHz, maxDoplerHz, 150);

minPulseRateHz = 125;
maxPulseRateHz = 800;
pulseRateHz = np.linspace (minPulseRateHz, maxPulseRateHz, 150);

[dopler, pulseRate] = np.meshgrid (doplerHz, pulseRateHz);

#              / w * T \ 2           / 2 * pi * f \
# K = 4 * sin | ------ |  = 4 * sin | ----------- |
#             \   2   /             \    2 * F   /
k = 4. * np.sin (np.pi * dopler / pulseRate ) ** 2;


fig = plt.figure ()
ax = fig.gca (projection = '3d')
surf = ax.plot_surface (dopler, pulseRate, k,
                        rstride = 1, cstride = 1, cmap = cm.coolwarm,
                        linewidth = 0, antialiased = False)

plt.title("Delay line canceller frequency response")
ax.set_xlabel("Dopler frequency")
ax.set_ylabel("Pulse repetition frequency")
ax.set_zlabel("Gain")
ax.view_init (50, 205);

plt.show()