import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack


time_of_view        = 10.; # s.

sampling_rate       = 100.; # Hz
sampling_period     = 1. / sampling_rate; # s
sample_number       = time_of_view / sampling_period;
sampling_time       = np.linspace (0, time_of_view, sample_number);


sampling_signal     = 9 * np.cos (2 * np.pi * 25 * sampling_time) \
                    + 3 * np.cos (2 * np.pi * 40 * sampling_time) \
                    + 5 * np.cos (2 * np.pi * 10 * sampling_time)

spectrum            = scipy.fftpack.fft (sampling_signal);
spectrum            = spectrum [0: round (sample_number / 2)]
spectrum            = (2. / sample_number) * spectrum
spectrum            = np.abs (spectrum);
spectrum            = spectrum ** 2
frequency_nyquist   = 1. / (2 * sampling_period)
frequency           = np.linspace (0., frequency_nyquist, sample_number / 2)

fig = plt.figure ()
plt.plot (frequency, spectrum);
#plt.semilogy (frequency, spectrum);
plt.title("Discrete Fourier transform")
plt.xlabel("Frequency")
plt.ylabel("Power $|F (x)|^2$")
plt.grid()

plt.show()