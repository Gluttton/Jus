import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
from mpl_toolkits.mplot3d import Axes3D


time_of_view        = 5.; # s.

sampling_rate       = 1000.; # Hz
sampling_period     = 1. / sampling_rate; # s
sample_number       = time_of_view / sampling_period;
sampling_time       = np.linspace (0, time_of_view, sample_number);


sampling_signal     = 10 * np.cos (2 * np.pi *  2 * sampling_time) \
                    +  5 * np.cos (2 * np.pi *  5 * sampling_time) \
                    +  1 * np.cos (2 * np.pi * 20 * sampling_time)

analytic_signal     = sig.hilbert (sampling_signal)



fig = plt.figure ()
ax = fig.gca (projection='3d')
ax.plot (sampling_time, np.real (analytic_signal), np.imag (analytic_signal), color='r', linewidth=1.)
ax.view_init (5, -170);

plt.title("Analytic signal.")
ax.set_xlabel("Time.")
ax.set_ylabel("Real part.")
ax.set_zlabel("Imaginary part.")

plt.show()