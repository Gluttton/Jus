import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack

time_of_view            = 1.
carrier_frequency       = 20
sampling_rate           = 2e3
sample_number           = time_of_view * sampling_rate
time                    = np.linspace (0, time_of_view, sample_number)

frequency_deviation     = 2 * (carrier_frequency - 1)
frequency_speed         = frequency_deviation / time_of_view
frequency_begin         = carrier_frequency - frequency_deviation / 2.
frequency_end           = carrier_frequency + frequency_deviation / 2.

signal                  = np.zeros (time.size);

for i in range (time.size):
    signal [i] = np.cos (2 * np.pi * (frequency_begin * time [i] + frequency_speed / 2 * time [i] ** 2) )

spectrum                = scipy.fftpack.fft (signal);
spectrum                = spectrum [0: int (sample_number / 2)]
spectrum                = (2. / sample_number) * spectrum
spectrum                = np.abs (spectrum);
spectrum                = spectrum ** 2
frequency_nyquist       = sampling_rate / 2.
frequency               = np.linspace (0., frequency_nyquist, sample_number / 2)


fig = plt.figure ()

plt.subplot (2, 1, 1)
plt.plot (time,  signal);
plt.title ("Signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")

plt.subplot (2, 1, 2)
plt.semilogy (frequency, spectrum, 'r');
plt.title ("Spectrum")
plt.xlabel ("Frequency")
plt.ylabel ("Power $|F (x)|^2$")
plt.grid ()

plt.show()
