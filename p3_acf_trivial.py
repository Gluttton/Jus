import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack

time_of_view            = 2.1e-3;
carrier_frequency       = 1e4;
sampling_rate           = 1e5;
sample_number           = int (time_of_view * sampling_rate);
sampling_time           = np.linspace (0, time_of_view, sample_number);

pulse_code_order        = 100;
pulse_code_width        = 1e-5;
pulse_width             = pulse_code_order * pulse_code_width;
pulse_sample_number     = int (pulse_width / (time_of_view / sampling_time.size) )
pulse_shift             = pulse_width * 1.;

signal_i                = np.random.normal (0, 0, sampling_time.size);
signal_q                = np.random.normal (0, 0, sampling_time.size);
filter_i                = np.zeros (sampling_time.size);
filter_q                = np.zeros (sampling_time.size);


for n in range (sampling_time.size):
    t = sampling_time [n]
    if t >= pulse_shift and t < pulse_shift + pulse_width:
        i = (t - pulse_shift) // pulse_code_width % pulse_code_order
        # Original.
        #(pi/N)(i-1)^2
        # Increment i to align its range.
        #(pi/N)((i+1)-1)^2
        #(pi/N)(i+1-1)^2
        #(pi/N)(i)^2
        # Result.
        #(pi/N)i^2
        phase =  (i ** 2) * np.pi / pulse_code_order
        signal_i [n] += np.cos (2 * np.pi * carrier_frequency * sampling_time [n] + phase)
        signal_q [n] += np.sin (2 * np.pi * carrier_frequency * sampling_time [n] + phase)

for n in range (pulse_sample_number):
    i = sampling_time [n] // pulse_code_width % pulse_code_order
    phase =  (i ** 2) * np.pi / pulse_code_order
    filter_i [pulse_sample_number - n] = np.sin (2 * np.pi * carrier_frequency * sampling_time [n] + phase)
    filter_q [pulse_sample_number - n] = np.cos (2 * np.pi * carrier_frequency * sampling_time [n] + phase)

spectrum_signal         = scipy.fftpack.fft (signal_i + 1j * signal_q);
spectrum_filter         = scipy.fftpack.fft (filter_i + 1j * filter_q);
signal_compressed       = scipy.fftpack.ifft (spectrum_signal * spectrum_filter)
magnitude_compressed    = np.absolute (signal_compressed)
magnitude_compressed    = np.roll (magnitude_compressed, -pulse_sample_number)
magnitude_compressed    = 20 * np.log10 (magnitude_compressed / pulse_code_order)


fig = plt.figure ()

plt.plot (sampling_time, magnitude_compressed);
plt.title ("Magnitude of compressed signal.")
plt.xlabel ("Time")
plt.ylabel ("Amplitude")
plt.ylim (-60, 0)

plt.show()
