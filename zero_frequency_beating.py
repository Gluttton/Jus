import numpy as np
import matplotlib.pyplot as plt


t = np.linspace (0, 1, 100000);
A = 1;
phi = 0;
f1 =  1005;
f2 =  1002;
U1 = A * np.cos (2 * np.pi * f1 * t + phi);
U2 = A * np.cos (2 * np.pi * f2 * t + phi);
Us = U1 + U2;


fig = plt.figure ()
#plt.plot (t, U1);
#plt.plot (t, U2);
plt.plot (t, Us);
plt.title("Zero frequency beating")
plt.xlabel("Time")
plt.ylabel("Amplitude")

plt.show()